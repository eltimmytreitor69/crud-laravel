<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta hacia la pagina principal       //Esta parte, indica el nombre que se utilizara en rutas Laravel
Route::get('/', 'PageController@inicio')->name('inicio');

//Ruta hacia la pagina de registro de usuarios
Route::get('/registrar', 'PageController@registrar')->name('registrar');

//Ruta hacia la pagina de registro de usuarios
Route::get('/registrar/{id}', 'PageController@detalle')->name('registrar.detalle');

//Ruta para crear usuarios
Route::post('/', 'PageController@crear')->name('registrar.crear');

//Ruta para editar usuarios
Route::get('/editar{id}', 'PageController@editar')->name('registrar.editar');

//Ruta para actualizar usuarios
Route::put('/editar{id}', 'PageController@actualizar')->name('registrar.actualizar');

//Ruta para eliminar
Route::delete('eliminar/{id}', 'PageController@eliminar')->name('registrar.eliminar');