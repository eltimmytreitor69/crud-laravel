@extends('plantilla')

@section('seccion')
    <h1>Detalle de Usuario</h1>

    <hr>

    <!-- div contenedor de la card -->
    <div class="card-group" style="margin: auto; width: 320px;">
        <div class="card">
            <img src="https://cdn.pixabay.com/photo/2017/06/10/07/24/note-2389227_960_720.png" class="card-img-top" alt="detalle.jpg">
            <!-- Cuerpo de la card-->
            <div class="card-body" style="height: 150px;">
                <p class="card-text h5"> Datos de <strong>{{ $registroUsuarios->nombre }}
                        {{ $registroUsuarios->apellido }}</strong> </p>
                        
                <!-- Boton ver más -->
                <button type="button" class="btn btn-secondary mb-5" data-toggle="modal" data-target="#staticBackdropLabel">
                    <span>Ver mas detalles</span>
                </button>
                <!-- Popup Modal -->
                <div class="modal fade" id="staticBackdropLabel" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <!-- Contenido del Modal-->
                        <div class="modal-content">
                            <!-- Header del Modal-->
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    <strong class="">Detalles de Usuario</strong>
                                </h5>
                                <!-- Boton cerrar ventana-->
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <!-- Body del Modal-->
                            <table class="table table-bordererd h6">
                                <thead class="bg-light">
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Apellido</th>
                                        <th scope="col">Correo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">{{ $registroUsuarios->id }}</th>
                                        <td>{{ $registroUsuarios->nombre }}</td>
                                        <td>{{ $registroUsuarios->apellido }}</td>
                                        <td>{{ $registroUsuarios->correo }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- FIN DEL MODAL-->
            </div>
        </div>
    </div>

    <!-- Enlace volver a registrar-->
    <div class="mt-4">
        <a href="{{ route('registrar') }}" class="h5 d-block" style="color: #777;">Volver</a>
    </div>
@endsection
