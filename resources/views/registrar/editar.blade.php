@extends('plantilla')

@section('seccion')
    <h1>Editar usuario {{ $editarUsuario->id }}</h1>

    <!-- ALERTA DE NOMBRE NO INGRESADO (CAMPO OBLIGATORIO)-->
    @error('nombre')
        <div class="alert alert-danger h6 text-left">
            <span>El nombre es obligatorio!</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @enderror
    <!-- ALERTA DE APELLIDO NO INGRESADO (CAMPO OBLIGATORIO)-->
    @error('apellido')
        <div class="alert alert-danger h6 text-left">
            <span>El apellido es obligatorio!</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @enderror
    <!-- ALERTA DE CORREO NO INGRESADO (CAMPO OBLIGATORIO)-->
    @error('correo')
        <div class="alert alert-danger h6 text-left">
            <span>El correo es obligatorio!</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @enderror


    <div class="col-md-5 m-auto">

        <div class="card card-body bg-light">
            <form action="{{ route('registrar.actualizar', $editarUsuario->id) }}" method="POST"
                class="form form-action mt-3">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <input type="text" name="nombre" placeholder="Nombre" class="form-control"
                        value="{{ $editarUsuario->nombre }}">
                </div>

                <div class="form-group">
                    <input type="text" name="apellido" placeholder="Apellido" class="form-control"
                        value="{{ $editarUsuario->apellido }}">
                </div>

                <div class="form-group">
                    <input type="text" name="correo" placeholder="Correo" class="form-control"
                        value="{{ $editarUsuario->correo }}">
                </div>
                <button class="btn btn-outline-secondary btn-block m-auto p-2" style="width: 150px;">Actualizar</button>
            </form>

        </div>

    </div>

    <!-- Redireccion a Registrar -->
    <div class="mt-4">
        <a href="{{ route('registrar') }}" class="h5 d-block" style="color: #777;">Volver</a>
    </div>
    
@endsection
