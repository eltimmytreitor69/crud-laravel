<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CRUD Laravel</title>

    <?php
    $iconoChayanne = 'https://cutt.ly/YWiAKQt';
    $iconoLaravel = 'https://cutt.ly/vWiAGUR';
    $iconoWalter = 'https://cutt.ly/eWiAZYg';
    ?>

    <!-- Icono Titulo -->
    <link rel="icon" href="{{ $iconoLaravel }}" type="image/x-icon">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>

</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif
        <!-- Titulo-->
        <div class="content">
            <div class="title m-b-md">
                CRUD
            </div>

            <!-- Links de redirección -->
            <div class="links">
                <a href="{{ route('inicio') }}">Inicio</a>
                <a href="{{ route('registrar') }}">Registro de usuarios</a>
                <a href="#repositoriosModal" data-toggle="modal">
                    <span>Repositorios</span>
                </a>

                <!-- Popup Modal-->
                <div class="modal fade" id="repositoriosModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <!-- Contenido del Popup-->
                        <div class="modal-content">
                            <!-- Header del Popup-->
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    <strong class="">Repositorios</strong>
                                </h5>
                                <!-- Boton cerrar ventana-->
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <!-- Body del Popup-->
                            <div class="modal-body">
                                <a href="https://github.com/eltimmytreitor69/CRUD-Laravel" target="_blank" class="m-5" style="color: #000;">
                                    <i class="fab fa-github m-1"></i>GitHub
                                </a>
                                <a href="https://bitbucket.org/eltimmytreitor69/crud-laravel/src" target="_blank" class="m-5" style="color: #000;">
                                    <i class="fab fa-bitbucket m-1"></i>BitBucket
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin del Modal-->
            </div>
        </div>
    </div>

    <!-- Bootstrap JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>
