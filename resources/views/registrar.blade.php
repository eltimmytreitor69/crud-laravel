@extends('plantilla')

@section('seccion')
    <!-- Titulo -->
    <h1 class="text-center mb-4">Registrar usuarios</h1>

    <hr class="w-50">

    <!-- Mensajes -->

    <!-- Mensaje CREADA -->
    @if (session('mensaje'))
        <div class="alert alert-success h6 text-left">
            {{ session('mensaje') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <!-- Mensaje EDITADO -->
    @if (session('mensajeEditado'))
        <div class="alert alert-warning h6 text-left">
            {{ session('mensajeEditado') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <!-- Mensaje ELIMINADO -->
    @if (session('mensajeEliminar'))
        <div class="alert alert-danger h6 text-left">
            {{ session('mensajeEliminar') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <!-- CREAR -->
    <form action="{{ route('registrar.crear') }}" method="POST" class="mt-3">
        @csrf
        <!-- Mensaje error campo Nombre vacio -->
        @error('nombre')
            <div class="alert alert-danger h6 text-left">
                <span>El nombre es obligatorio!</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror

        <!-- Mensaje error campo Apellido vacio -->
        @error('apellido')
            <div class="alert alert-danger h6 text-left">
                <span>El apellido es obligatorio!</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror

        <!-- Mensaje error campo Correo vacio -->
        @error('correo')
            <div class="alert alert-danger h6 text-left">
                <span>El correo es obligatorio!</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror

        <!-- Inputs -->
        <div class="col-md-5 m-auto">
            <div class="card card-body bg-light">
                <!-- Input Nombre -->
                <input type="text" name="nombre" placeholder="Nombre" class="form-control mb-3 p-3"
                    value="{{ old('nombre') }}">
                <!-- Input Apellido -->
                <input type="text" name="apellido" placeholder="Apellido" class="form-control mb-3 p-3"
                    value="{{ old('apellido') }}">
                <!-- Input Correo -->
                <input type="text" name="correo" placeholder="Correo" class="form-control mb-3 p-3"
                    value="{{ old('correo') }}">

                <!-- Boton AGREGAR -->
                <div class="m-auto">
                    <button class="btn btn-success btn-block mt-4 p-2" style="width: 150px;" type="submit">Agregar</button>
                </div>
            </div>
        </div>
    </form>

    <!-- Tabla Usuarios -->
    <table class="table table-hover h6 mt-4">
        <!-- Cabecera de la tabla -->
        <thead class="thead-light">
            <!-- Contenedor del contenido de la cabecera -->
            <tr>
                <!-- Contenido de la cabecera-->
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Correo</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <!-- Cuerpo de la tabla -->
        <tbody>
            <!-- foreach recorre la tabla registro de la base de datos segun cantidad de id tenga -->
            @foreach ($registroUsuarios as $i)
                <!-- Contenedor del contenido de la cuerpo-->
                <tr>
                    <!-- Contenido del cuerpo -->
                    <th scope="row">{{ $i->id }}</th>
                    <td>
                        <!-- Redireccion a Detalle -->
                        <a href="{{ route('registrar.detalle', $i) }}" style="color:#555; text-decoration: underline;">
                            {{ $i->nombre }}
                        </a>
                    </td>
                    <td>{{ $i->apellido }}</td>
                    <td>{{ $i->correo }}</td>
                    <td>
                        <!-- Boton Editar -->
                        <a href="{{ route('registrar.editar', $i) }}" class="btn btn-secondary">
                            <i class="fas fa-marker d-inline"></i>
                        </a>
                        <!-- Boton abrir Modal Eliminar -->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarBackdrop"
                            data-id="{{ $i->id }}">
                            <i class="fas fa-trash-alt"></i>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="eliminarBackdrop" data-backdrop="static" data-keyboard="false"
                            tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <!-- Contenido del Modal -->
                                <div class="modal-content">
                                    <!-- Header del Popup -->
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            <strong class="text-danger">Eliminar usuario {{ $i->id }}</strong>
                                        </h5>
                                        <!-- Boton cerrar ventana -->
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <!-- Body del Popup -->
                                    <div class="modal-body h5">
                                        ¿Estas seguro que deseas <strong class="text-danger">eliminar</strong> este usuario?
                                    </div>
                                    <!-- Footer del Popup -->
                                    <div class="modal-footer">
                                        <!-- Boton Cerrar-->
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Volver
                                        </button>
                                        <!-- Form ELIMINAR -->
                                        <form id="formEliminar" action="{{ route('registrar.eliminar', 1) }}"
                                            data-action="{{ route('registrar.eliminar', 1) }}" method="POST"
                                            class="d-inline">
                                            @method('DELETE')
                                            @csrf
                                            <!-- Boton ELIMINAR -->
                                            <button class="btn btn-danger" type="submit">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIN del MODAL -->

                        <!-- SCRIPT MODAL -->
                        <script>
                            $('#eliminarBackdrop').on('show.bs.modal', function(event) {
                                var button = $(event.relatedTarget) // Boton que acciona el evento Modal
                                var id = button.data('id') //Captura id con el nombre en etiqueta, y se almacena en variable

                                //Captura id de formulario eliminar y data-action obtiene la ruta junto al id del usuario // attr se usa para acceder al atributo
                            
                                action = $('#formEliminar').attr('data-action').slice(0, -1);
                                //0, -1 le quita el valor definido en action (1 en este caso)

                                //slice() devuelve una copia de una parte del array dentro de un nuevo array (desdel incio a fin). El array original no se modifica.

                                //action = action + id;
                                action += id;

                                //attr se usa para acceder al atributo
                                //Obtiene el atributo action de el formulario eliminar
                                $('#formEliminar').attr('action', action);

                                //Se muestra por consola los datos de variable action
                                console.log(action);
                                var modal = $(this)

                                //Titulo de la modal
                                modal.find('.modal-title').text('Eliminar usuario ' + id)
                            })
                        </script>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
