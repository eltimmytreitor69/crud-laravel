<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PageController extends Controller
{
    //Funcion que retorna la blade de welcome
    public function inicio(){
        return view('welcome');
    }

    //Funcion que retorna la blade de registrar
    public function registrar(){
        //Variable que almacena todos los datos de la BDD
        $registroUsuarios = App\Registro::all();
                                         //Este nombre debe ser igual al de la variable
        return view('registrar', compact('registroUsuarios'));
    }                           //compacto evita duplicidad de datos

    public function detalle($id){
        $registroUsuarios = App\Registro::findOrFail($id);

        return view('registrar.detalle', compact('registroUsuarios'));
    }

    public function crear(Request $request){
        $request->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'correo' => 'required'
        ]);

        $registroNuevo = new App\Registro;
        $registroNuevo->nombre = $request->nombre;
        $registroNuevo->apellido = $request->apellido;
        $registroNuevo->correo = $request->correo;

        $registroNuevo->save();

        return back()->with('mensaje', 'Usuario creado correctamente!');

        /* Retorna todos los valores que se escriban en los inputs y el token // Sirve como verificador
        return $request->all();
        */
    }

    public function editar($id){
        $editarUsuario = App\Registro::findOrFail($id);

        return view('registrar.editar',compact('editarUsuario'));
    }

    public function actualizar(Request $request, $id){
        $request->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'correo' => 'required'
        ]);

        $actualizarUsuario = App\Registro::findOrFail($id);

        $actualizarUsuario->nombre = $request->nombre;
        $actualizarUsuario->apellido = $request->apellido;
        $actualizarUsuario->correo = $request->correo;

        $actualizarUsuario->save();

        //Redirije a la pagina (notas) y envia mensaje de edicion completada correctamente
        return redirect('registrar')->with('mensajeEditado', 'Usuario editado correctamente!');

    }

    public function eliminar($id){
        $eliminarUsuario = App\Registro::findOrFail($id);
        $eliminarUsuario->delete();

        return back()->with('mensajeEliminar', 'Usuario eliminado correctamente!');
        
    }

}
